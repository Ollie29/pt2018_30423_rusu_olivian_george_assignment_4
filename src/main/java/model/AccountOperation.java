package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class AccountOperation implements Serializable {
    private LocalDate date;
    private boolean withdrawal;   // true if it is a withdrawal, 
                                    // false if it is a deposit
    private float amount;
    
    public AccountOperation(LocalDate date, boolean withdrawal, float amount) {
        this.date = date;
        this.withdrawal = withdrawal;
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public boolean isWithdrawal() {
        return withdrawal;
    }

    public float getAmount() {
        return amount;
    }
    
    @Override
    public String toString() {
        String result;
        if (withdrawal)
            result = "Withdrawn: " + amount + " on " + date;
        else
            result = "Deposited: " + amount + " on " + date;
        return result;
    }

    void setDate(LocalDate date) {
        this.date = date;
    }

    void setWithdrawal(boolean withdrawal) {
        this.withdrawal = withdrawal;
    }

    void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) { //just to be sure, not sure if identified op when edit
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AccountOperation other = (AccountOperation) obj;
        if (this.withdrawal != other.withdrawal) {
            return false;
        }
        if (Float.floatToIntBits(this.amount) != Float.floatToIntBits(other.amount)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    private void readObject(ObjectInputStream in)
        throws IOException, ClassNotFoundException {
        date = (LocalDate)in.readObject();
        withdrawal = in.readBoolean();
        amount = in.readFloat();
    }

    private void writeObject(ObjectOutputStream out)
     throws IOException {
        out.writeObject(date);
        out.writeBoolean(withdrawal);
        out.writeFloat(amount);
    }
}
