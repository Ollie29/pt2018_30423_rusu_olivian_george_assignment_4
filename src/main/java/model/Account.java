package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable, Comparable<Account> {

    protected String iban;
    protected ArrayList<AccountOperation> operations;

    public Account(String iban) {
        this.iban = iban;
        operations = new ArrayList<>();
    }

    public String getIban() {
        return iban;
    }

    public abstract boolean addOperation(AccountOperation op);

    public boolean deleteOperation(AccountOperation op) {
        if (op == null) {
            return false;
        }
        if (operations.contains(op)) {
            operations.remove(op);
            if (getBalance() < 0) {     // cannot withdraw more money than deposited (plus interest)
                operations.add(op);
            } else {
                setChanged();
                notifyObservers("Account: " + iban + "  Cancelled operation " + op.toString());
                return true;
            }
        }
        return false;
    }

    public Double getBalance() {
        double balance = 0.0;
        for (AccountOperation o : operations) {
            if (o.isWithdrawal()) {
                balance -= o.getAmount();
            } else {
                balance += o.getAmount();
            }
        }
        return balance;
}

    @Override
    public String toString() {
        return iban;
    }

    protected void readObject(ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        iban = in.readUTF();
        operations = (ArrayList<AccountOperation>) in.readObject();
    }

    protected void writeObject(ObjectOutputStream out)
            throws IOException {
        out.writeUTF(iban);
        out.writeObject(operations);
    }

    public void changeOperation(AccountOperation op, LocalDate newDate,
            boolean newWithdrawal, float newAmount) {
        if (op == null) {
            return;
        }
        if (operations.contains(op)) {
            String message = "Account: " + iban + "  Changed operation " + op.toString();
            op.setAmount(newAmount);
            op.setDate(newDate);
            op.setWithdrawal(newWithdrawal);
            setChanged();
            notifyObservers(message + " to " + op.toString());
        }
    }

    public ArrayList<AccountOperation> getOperations() {
        return operations;
    }

    @Override
    public boolean equals(Object obj) { //account comparing for adding to TreeSet
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.iban, other.iban)) {
            return false;
        }
        return true;
    }

    void notifyDelete() {
        setChanged();
        notifyObservers("Account " + iban + " deleted");
    }

    @Override
    public int compareTo(Account acc) { //for TreeSet
        if (acc == null)
            throw new NullPointerException();
        return iban.compareTo(acc.getIban());
    }
    
}
