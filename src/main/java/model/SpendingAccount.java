package model;

public class SpendingAccount extends Account {

    public SpendingAccount(String iban) {
        super(iban);
    }

    @Override
    public boolean addOperation(AccountOperation op) {
        if (op != null) {
            operations.add(op);
            if (getBalance() < 0) {     // cannot withdraw more money than deposited (plus interest)
                operations.remove(op);
            } else {
                setChanged();
                notifyObservers("Account: " + iban + "  " + op.toString());
                return true;
            }
        }
        return false;
    }
}
