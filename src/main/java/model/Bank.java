package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Bank implements BankProc, Serializable {

    private HashMap<Person, TreeSet<Account>> accounts;

    public Bank() {
        accounts = new HashMap<>();
    }

    public Set<Person> getClients() {
        return accounts.keySet();
    }

    @Override
    public void addClient(Person p) {
        assert isWellFormed();
        assert p != null;
        assert !isClient(p);
        int noOfClientsPre = getNoOfClients();
        accounts.put(p, new TreeSet<>());
        assert noOfClientsPre + 1 == getNoOfClients();
        assert isClient(p);
        assert getNoOfAccounts(p) == 0;
        assert isWellFormed();
    }

    @Override
    public void deleteClient(Person p) {
        assert isWellFormed();
        assert p != null;
        assert isClient(p);
        int noOfClientsPre = getNoOfClients();
        //accounts.get(p).clear();
        for (Account acc: accounts.get(p))
            deleteAccount(p, acc);
        accounts.remove(p);
        assert !isClient(p);
        assert noOfClientsPre - 1 == getNoOfClients();
        assert getNoOfAccounts(p) == 0;
        assert isWellFormed();
    }

    @Override
    public boolean isClient(Person p) {
        assert isWellFormed();
        assert p != null;
        return accounts.containsKey(p);
    }

    @Override
    public int getNoOfClients() {
        // no need for assert
        return accounts.size(); // number of key-value mappings
    }

    @Override
    public void addAccount(Person p, Account a) {
        assert isWellFormed();
        assert p != null;
        assert isClient(p);
        assert a != null && !belongsTo(a, p) && !belongsToAnyone(a);
        accounts.get(p).add(a);
        a.addObserver(p);
        assert belongsTo(a, p);
        assert isWellFormed();
    }

    @Override
    public void deleteAccount(Person p, Account a) {
        assert p != null && a != null;
        assert belongsTo(a, p) && a.getBalance().equals(0);
        a.notifyDelete();
        accounts.get(p).remove(a);
        a.deleteObserver(p);
        assert !belongsToAnyone(a);
    }

    @Override
    public int getNoOfAccounts() {
        // no need for assert
        return accounts.values().size();
    }

    @Override
    public int getNoOfAccounts(Person p) {
        assert isWellFormed();
        assert p != null;
        assert isClient(p);
        return accounts.get(p).size();
    }

    @Override
    public boolean belongsTo(Account a, Person p) {
        assert  isWellFormed();
        assert p != null && isClient(p) && a != null;
        return accounts.get(p).contains(a);
    }

    @Override
    public boolean belongsToAnyone(Account a) {
        assert isWellFormed();
        assert a != null;
        for (TreeSet<Account> list : accounts.values()) { //iterates through set of accounts
            if (list.contains(a)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void read(File chosen) {
        assert chosen != null;
        FileInputStream file = null;
        try {
            file = new FileInputStream(chosen);//
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Bank.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (file == null)
            return;
        try {
            ObjectInputStream in = new ObjectInputStream(file);//
            readObject(in);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Bank.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void write(File chosen) {
        FileOutputStream file = null;
        try {
            file = new FileOutputStream(chosen);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Bank.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (file == null)
            return;
        try {
            ObjectOutputStream out = new ObjectOutputStream(file);
            writeObject(out);//
        } catch (IOException ex) {
            Logger.getLogger(Bank.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeObject(ObjectOutputStream out)
            throws IOException {
        out.writeObject(accounts);
    }

    private void readObject(ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        accounts = (HashMap<Person, TreeSet<Account>>) in.readObject();
        for (Person p : accounts.keySet()) { //after reading it needs to remap the observers
            for (Account acc : accounts.get(p)) {
                acc.addObserver(p);
            }
        }
    }

    public ArrayList<Account> getAccounts(Person p) {
        if (isClient(p)) {
            Object[] obj = accounts.get(p).toArray();
            ArrayList<Account> acc = new ArrayList<>();
            for (Object o : obj) {
                acc.add((Account) o);
            }
            return acc;
        }
        return null;
    }

    @Override
    public boolean isWellFormed() {
        if (accounts == null)
            return false;
        for (Person p: accounts.keySet()) {
            for (Account acc: accounts.get(p))
                if (acc.countObservers() != 1)
                    return false;
        }
        return true;
    }

    @Override
    public void changeClientName(Person p, String newName) {
        assert isWellFormed();
        assert p!=null && newName != null && !newName.equals("") && isClient(p);
        int nrAcc = getNoOfAccounts(p);
        ArrayList<Account> accts = getAccounts(p);
        accounts.remove(p);
        p.setName(newName);
        addClient(p);
        for (Account acc: accts)
            addAccount(p, acc);
        assert isClient(p) && getNoOfAccounts(p) == nrAcc;
        assert isWellFormed();
    }

}