package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable{
    String name;
    StringBuffer log;

    public Person(String name) {
        this.name = name;
        log = new StringBuffer();
    }

    void setName(String name) {
        if (name != null)
            this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getLog() {
        return log.toString();
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public void update(Observable o, Object arg) {
        String s = (String) arg;
        log.append(s).append("\n");
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    private void readObject(ObjectInputStream in)
        throws IOException, ClassNotFoundException {
        name = in.readUTF();
        log = new StringBuffer(in.readUTF());
    }
    
    private void writeObject(ObjectOutputStream out)
     throws IOException {
        out.writeUTF(name);
        out.writeUTF(log.toString());
    }
}
