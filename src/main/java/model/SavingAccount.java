package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SavingAccount extends Account {

    private double interest;

    public SavingAccount(String iban, double interest) {
        super(iban);
        if (interest > 30 || interest < 0) // say the maximum interest could be 30%
        {
            interest = 0;
        }
        this.interest = interest;
    }

    public double getInterest() {
        return interest;
    }

    @Override
    public boolean addOperation(AccountOperation op) {
        // The saving account allows a single large sum deposit and withdrawal
        if (operations.size() >= 2 || op == null) {
            return false;
        }
        if (operations.isEmpty()
                || (operations.get(0).isWithdrawal() != op.isWithdrawal())) {
            // add the operation only if there were none before it or if 
            // the one before was of different type
            double eps = 1e-5;
            if (op.isWithdrawal() && Math.abs(getBalance()-op.getAmount()) < eps) {      
                // cannot withdraw more money than deposited (plus interest)
                operations.remove(op);
            } else {
                operations.add(op);
                setChanged();
                notifyObservers("Account: " + iban + "  " + op.toString());
                return true;
            }
        }
        return false;
    }

    @Override
    public Double getBalance() {
        double balance = 0.0;   //= super.getBalance();
        for (AccountOperation op : operations)
            if (!op.isWithdrawal())
                balance += op.getAmount();
        balance += balance * interest/100;
        for (AccountOperation op : operations)
            if (op.isWithdrawal())
                balance -= op.getAmount();
        return balance;
    }

    @Override
    protected void readObject(ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        super.readObject(in);
        interest = in.readDouble();
    }

    @Override
    protected void writeObject(ObjectOutputStream out)
            throws IOException {
        super.writeObject(out);
        out.writeDouble(interest);
    }
}
