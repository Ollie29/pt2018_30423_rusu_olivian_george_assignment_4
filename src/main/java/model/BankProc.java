package model;

import java.io.File;

/**
 *
 * @invariant isWellFormed()
 */
public interface BankProc {
    /**
     * Add a new client to the bank
     * @param p The client to be added to the bank
     * @pre p!=null && !isClient(p)
     * @post getNoOfClients() == getNoOfClients()@pre + 1
     * @post isClient(p)
     * @post getNoOfAccounts(p) == 0
     */
    public void addClient(Person p);
    
    /**
     * Remove a client and all its accounts
     * @param p The client to be deleted
     * @pre p!=null && isClient(p)
     * @post getNoOfClients() == getNoOfClients()@pre - 1 
     * @post !isClient(p)
     * @post getNoOfAccounts(p) == 0
     */
    public void deleteClient(Person p);

    /**
     * Change the name of a client
     * @param p the client whose name is to be changed
     * @param newName the new name
     * @pre p!=null && newName != null && !newName.equals("") && isClient(p)
     * @post isClient(p) && getNoOfAccounts(p) == getNoOfAccounts(p@pre)
     */
    public void changeClientName(Person p, String newName);

    
    /**
     * Check if p is a client of the bank
     * @param p the client to be checked
     * @return true if p is a client of the bank
     * @pre p != null
     * @post @nochange
     */
    public boolean isClient(Person p);
    
    /**
     * Obtain the number of clients of the bank
     * @return the number of clients of the bank
     * @pre true
     * @post @nochange
     */
    public int getNoOfClients();
    
    /**
     * Associates an account to a client
     * @param p the client 
     * @param a the account
     * @pre p != null && isClient(p)
     * @pre a != null && !belongsTo(a, p) && !belongsToAnyone(a)
     * @post belongsTo(a,p)
     */
    public void addAccount(Person p, Account a);
    
    /**
     * Closes an account of a client (only if the account balance is 0)
     * @param p the client
     * @param a the account
     * @pre p != null && a != null
     * @pre belongsTo(a, p) && a.getBalance().equals(0)
     * @post !belongsToAnyone(a)
     */
    public void deleteAccount(Person p, Account a);
    
    /**
     * Obtain the number of opened accounts in the bank
     * @return the total number of accounts opened in the bank
     * @pre true
     * @post @nochange
     */
    public int getNoOfAccounts();
    
    /**
     * Obtain the number of opened accounts for a client of the bank
     * @param p the client
     * @return the number of opened accounts for the specified client
     * @pre p != null && isClient(p)
     * @post @nochange
     */
    public int getNoOfAccounts(Person p);
    
    /**
     * Check if an account belongs to a specific client
     * @param a the account
     * @param p the client
     * @return true if the account belongs to the client; false otherwise
     * @pre a != null && p != null && isClient(p)
     * @post @nochange
     */
    public boolean belongsTo(Account a, Person p);
    
    /**
     * Check if an account belongs to any client
     * @param a the account
     * @return true if the account belongs to some client of the bank; false otherwise
     * @pre a != null
     * @post @nochange
     */
    public boolean belongsToAnyone(Account a);
    
    /**
     * Reads the data of the bank from a file
     * @param file the file
     * @pre file != null
     * @post
     */
    public void read(File file);
    
    /**
     * Saves the data of the bank to a file
     * @param file the file
     * @pre file != null
     * @post
     */
    public void write(File file);
    
    /**
     * Checks the integrity of the bank data (the invariant of the class)
     * @return true if the data is correct, false otherwise
     * @pre true
     * @post @nochange
     */
    public boolean isWellFormed();
}
