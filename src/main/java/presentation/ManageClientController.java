package presentation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;

public class ManageClientController {

    @FXML
    private TableColumn<Account, String> typeCol;

    @FXML
    private ComboBox<Person> client;

    @FXML
    private TableColumn<Account, String> ibanCol;

    @FXML
    private TableColumn<Account, String> balanceCol;

    @FXML
    private TableColumn<Account, String> interestCol;

    @FXML
    private TextField nameField;

    @FXML
    private TableView<Account> accountsTable;

    private Bank bank;

    @FXML
    void deleteClient(ActionEvent event) {
        Person p = client.getSelectionModel().getSelectedItem();
        if (p == null) {
            return;
        }
        double balance = 0;
        double eps = 1e-5;
        // compute the balance for all acounts; if it is not 0 then don't delete client
        for (Account acc : accountsTable.getItems()) {
            balance += acc.getBalance();
        }
        if (balance > eps) {    // is not close enough to 0
            MainWindowController.displayInfo("Must withdraw first all money from accounts; "
                    + balance + " left in the accounts.", "", "Cannot delete client");
        } else {
            bank.deleteClient(p);
            accountsTable.getItems().clear();
            client.getItems().remove(p);
            client.getSelectionModel().clearSelection();
        }
    }

    @FXML
    void cancel(ActionEvent event) {
        Stage stage = (Stage) nameField.getScene().getWindow();
        stage.hide();
    }

    @FXML
    void addClient(ActionEvent event) {
        String name = nameField.getText().trim();
        if (name.equals("")) {
            return;
        }
        Person p = new Person(name);
        if (bank != null && !bank.isClient(p)) {
            bank.addClient(p);
            client.getItems().add(p);
        }
        nameField.clear();
    }

    @FXML
    void showNotifications(ActionEvent event) {
        Person p = client.getSelectionModel().getSelectedItem();
        if (p == null) {
            return;
        }
        String s = p.getLog();
        if (s == null || s.equals("")) {
            s = "No notifications.";
        }
        MainWindowController.displayInfo(s, "",
                "Notifications received by client " + p.getName());
    }

    @FXML
    void initialize() {
        initListClients();
        client.getSelectionModel().selectedItemProperty().addListener(
                (object, oldValue, newValue) -> {
                    Person p = (Person) newValue;
                    initAccountsTable(p);
                }
        );
        initAccountsTableProperties();
    }

    private void initListClients() {
        if (bank != null) {
            client.getItems().clear();
            client.getItems().addAll(bank.getClients());
        }
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    private void initAccountsTableProperties() {
        ibanCol.setCellValueFactory(
                new PropertyValueFactory<Account, String>("iban")
        );
        typeCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                String s = a.getValue().getClass().toString();
                if (s.contains("Saving")) {
                    return new SimpleStringProperty("saving");
                } else {
                    return new SimpleStringProperty("spending");
                }
            }
        });
        balanceCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                double balance = a.getValue().getBalance();
                return new SimpleStringProperty("" + balance);
            }
        });
        interestCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                Account acc = a.getValue();
                if (acc instanceof SavingAccount) {
                    return new SimpleStringProperty(""
                            + ((SavingAccount) acc).getInterest());
                } else {
                    return new SimpleStringProperty(" - ");
                }
            }
        });

    }

    private void initAccountsTable(Person p) {
        accountsTable.getItems().clear();
        if (p != null) {
            accountsTable.getItems().addAll(bank.getAccounts(p));
        }
    }

    void initializeaza() {
        initListClients();
        nameField.setText("");
    }
}
