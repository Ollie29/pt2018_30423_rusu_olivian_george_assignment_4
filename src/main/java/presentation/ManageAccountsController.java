package presentation;

import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

public class ManageAccountsController {

    @FXML
    private ComboBox<Person> client;
    @FXML
    private TableView<Account> accountsTable;
    @FXML
    private TableColumn<Account, String> ibanCol;
    @FXML
    private TableColumn<Account, String> typeCol;
    @FXML
    private TableColumn<Account, String> balanceCol;
    @FXML
    private TableColumn<Account, String> interestCol;
    @FXML
    private TextField ibanField;
    @FXML
    private ComboBox<String> accountTypeField;
    @FXML
    private TableView<AccountOperation> operationsTable;
    @FXML
    private TableColumn<AccountOperation, LocalDate> dateCol;
    @FXML
    private TableColumn<AccountOperation, String> actionCol;
    @FXML
    private TableColumn<AccountOperation, Float> amountCol;
    @FXML
    private DatePicker dateField;
    @FXML
    private ComboBox<String> actionField;
    @FXML
    private TextField amountField;
    @FXML
    private TextField interestField;
    @FXML
    private VBox interestContainer;

    Bank bank;

    @FXML
    void initialize() {
        initTypeField();
        actionField.getItems().addAll("Deposit", "Withdrawal");

        client.getSelectionModel().selectedItemProperty().addListener(
                (object, oldValue, newValue) -> {
                    Person p = (Person) newValue;
                    initAccountsTable(p);
                }
        );

        initAccountsTableProperties();
        initOperationsTableProperties();
    }

    @FXML
    private void deleteAccount(ActionEvent event) {
        Account acc = accountsTable.getSelectionModel().getSelectedItem();
        if (acc == null) {
            return;
        }
        double balance = acc.getBalance();  // should be positive
        double eps = 1e-5;
        if (balance > eps) {    // is not close enough to 0
            MainWindowController.displayInfo("Must withdraw first all money; "
                    + balance + " left in account.", "", "Cannot delete account");
        } else {
            Person p = client.getSelectionModel().getSelectedItem();
            // p != null certainly, if an account is selected

            bank.deleteAccount(p, acc);
            accountsTable.getItems().remove(acc);
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) amountField.getScene().getWindow();
        stage.hide();
    }

    @FXML
    private void addAccount(ActionEvent event) {
        Person p = client.getSelectionModel().getSelectedItem();
        if (p == null) {// nothing selected
            MainWindowController.displayInfo("You have to select the client first!",
                    "", "Cannot add account");
            return;
        }
        String iban = ibanField.getText().trim();
        if (iban.equals("")) {
            return;
        }
        String type = accountTypeField.getSelectionModel().getSelectedItem();
        Account acc = null;
        if (type.contains("Spending")) {
            acc = new SpendingAccount(iban);
        } else {
            String text = interestField.getText().trim();
            if (text.equals("")) {
                return;
            }
            try {
                float interest = Float.parseFloat(text);
                if (interest < 0 || interest > 30) {
                    MainWindowController.displayInfo("Interest between 0 and 30%",
                            "", "Wrong value for interest");
                    return;
                }
                acc = new SavingAccount(iban, interest);
            } catch (NumberFormatException e) {
                MainWindowController.displayInfo("Interest has to be a real number",
                        "", "Wrong value for interest");
            }
        }
        if (acc != null) {
            bank.addAccount(p, acc);
            accountsTable.getItems().add(acc);
        }

        ibanField.setText("");
        interestField.setText("");
        accountTypeField.getSelectionModel().select(0);

    }

    @FXML
    private void addOperation(ActionEvent event) {
        Account a = accountsTable.getSelectionModel().getSelectedItem();
        if (a == null) {
            return;
        }
        LocalDate d = dateField.getValue();
        if (d == null) {
            return;
        }
        String text = amountField.getText().trim();
        if (text.equals("")) {
            return;
        }
        float amount = -1;
        try {
            amount = Float.parseFloat(text);
        } catch (NumberFormatException e) {
        }
        if (amount <= 0) {
            MainWindowController.displayInfo("Amount should be a strictly positive float",
                    "", "Wrong value for amount");
            return;
        }
        boolean withdrawal = actionField.getSelectionModel().isSelected(1);
        if (withdrawal) {   // cannot withdraw more than the account balance
            if (a.getBalance() - amount < 0) {
                MainWindowController.displayInfo("Amount withdrawn cannot be more"
                        + " than the balance", "", "Can't do operation");
                return;
            }
        }
        AccountOperation op = new AccountOperation(d, withdrawal, amount);
        if (a.addOperation(op)) {
            operationsTable.getItems().add(op);
            clearOperationsFields();
            updateAccountsTable();
        } else {
            MainWindowController.displayInfo("Cannot add the operation",
                    "", "Can't add operation");
        }
    }

    @FXML
    private void editOperation(ActionEvent event) {
        AccountOperation op = operationsTable.getSelectionModel().getSelectedItem();
        int i = operationsTable.getItems().indexOf(op);
        if (op == null) {
            return;
        }
        LocalDate d = dateField.getValue();
        if (d == null) {
            return;
        }
        String text = amountField.getText().trim();
        if (text.equals("")) {
            return;
        }
        float amount = -1;
        try {
            amount = Float.parseFloat(text);
        } catch (NumberFormatException e) {
        }
        if (amount <= 0) {
            MainWindowController.displayInfo("Amount should be a strictly positive float",
                    "", "Wrong value for amount");
            return;
        }
        boolean withdrawal = actionField.getSelectionModel().isSelected(1);
        
        if (withdrawal == op.isWithdrawal() && amount == op.getAmount() &&
                d.equals(op.getDate()))
            return;     // nothing changed
        Account a = accountsTable.getSelectionModel().getSelectedItem();
        if (withdrawal) {   // cannot withdraw more than the account balance    
            double balance = a.getBalance();
            // compute the balance without operation op:
            if (op.isWithdrawal()) {
                balance += op.getAmount();
            } else {
                balance -= op.getAmount();
            }
            if (balance - amount < 0) {
                MainWindowController.displayInfo("Amount withdrawn cannot be more"
                        + " than the balance", "", "Can't do operation");
                return;
            }
        }
        a.changeOperation(op, d, withdrawal, amount);
        operationsTable.getItems().set(i, op);
        updateAccountsTable();
        clearOperationsFields();
    }

    @FXML
    private void deleteOperation(ActionEvent event) {
        Account acc = accountsTable.getSelectionModel().getSelectedItem();
        if (acc == null) {
            return;
        }
        AccountOperation op = operationsTable.getSelectionModel().getSelectedItem();
        if (op == null) {
            return;
        }
        double balance = acc.getBalance();
        // if deleting a deposit operation, check if the account balance would be negative:
        if (!op.isWithdrawal()) {
            balance -= op.getAmount();
            if (balance < 0) {
                MainWindowController.displayInfo("Deleting this operation would"
                        + " leave negative balance in the account.", "", "Cannot delete operation");
                return;
            }
        }
        if (acc.deleteOperation(op)) {
            operationsTable.getItems().remove(op);
            updateAccountsTable();
            clearOperationsFields();
        } else {
            MainWindowController.displayInfo("Cannot delete the operation",
                    "", "Cannot delete ");
        }
    }

    void initializeaza() {
        initListClients();
        accountTypeField.getSelectionModel().select(0);
        interestContainer.setVisible(false);
        actionField.getSelectionModel().select(0);
        accountsTable.getItems().clear();
        operationsTable.getItems().clear();
    }

    private void initListClients() {
        if (bank != null) {
            client.getItems().clear();
            client.getItems().addAll(bank.getClients());
        }
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    private void initAccountsTable(Person p) {
        if (p != null) {
            accountsTable.getItems().clear();
            accountsTable.getItems().addAll(bank.getAccounts(p)); // also calculates balance again
            operationsTable.getItems().clear();
        }
    }

    private void initTypeField() {
        accountTypeField.getItems().addAll("Spending account", "Savings account");
        accountTypeField.getSelectionModel().selectedItemProperty().addListener(
                (obiect, valVeche, valNoua) -> {
                    String s = (String) valNoua;
                    if (s.contains("Spending")) {
                        interestContainer.setVisible(false);
                    } else {
                        interestContainer.setVisible(true);
                    }
                }
        );
    }

    private void initAccountsTableProperties() {
        ibanCol.setCellValueFactory(
                new PropertyValueFactory<Account, String>("iban")
        );
        typeCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                String s = a.getValue().getClass().toString();
                if (s.contains("Saving")) {
                    return new SimpleStringProperty("saving");
                } else {
                    return new SimpleStringProperty("spending");
                }
            }
        });
        balanceCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                double balance = a.getValue().getBalance();
                return new SimpleStringProperty("" + balance);
            }
        });
        interestCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Account, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Account, String> a) {
                // a.getValue() returns the Account instance for a particular TableView row
                Account acc = a.getValue();
                if (acc instanceof SavingAccount) 
                    return new SimpleStringProperty("" + 
                            ((SavingAccount)acc).getInterest() + "%");
                else
                    return new SimpleStringProperty(" - ");
        }});

        accountsTable.getSelectionModel().selectedItemProperty().addListener(
                (object, oldValue, newValue) -> {
                    Account acc = (Account) newValue;
                    initOperationsTable(acc);
                }
        );
    }

    private void initOperationsTable(Account acc) {
        if (acc != null) {
            operationsTable.getItems().clear();
            operationsTable.getItems().addAll(acc.getOperations());
        }
    }

    private void initOperationsTableProperties() {
        dateCol.setCellValueFactory(
                new PropertyValueFactory<AccountOperation, LocalDate>("date")
        );
        actionCol.setCellValueFactory(new Callback<CellDataFeatures<AccountOperation, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<AccountOperation, String> a) {
                // a.getValue() returns the AccountOperation instance for a particular TableView row
                boolean withdraw = a.getValue().isWithdrawal();
                if (withdraw) {
                    return new SimpleStringProperty("withdraw");
                } else {
                    return new SimpleStringProperty("deposit");
                }
            }
        });
        amountCol.setCellValueFactory(
                new PropertyValueFactory<AccountOperation, Float>("amount")
        );

        operationsTable.getSelectionModel().selectedItemProperty().addListener(
                (object, oldValue, newValue) -> {
                    AccountOperation op = (AccountOperation) newValue;
                    initOperationsFields(op);
                }
        );
    }

    private void initOperationsFields(AccountOperation op) {
        if (op != null) {
            amountField.setText("" + op.getAmount());
            dateField.setValue(op.getDate());
            actionField.getSelectionModel().select(op.isWithdrawal() ? 1 : 0);
        }
    }

    private void clearOperationsFields() {
        dateField.setValue(null);
        actionField.getSelectionModel().clearSelection();
        amountField.setText("");
    }

    private void updateAccountsTable() {
        int i = accountsTable.getSelectionModel().getSelectedIndex();
        Person p = client.getSelectionModel().getSelectedItem();
        initAccountsTable(p);
        accountsTable.getSelectionModel().select(i);
    }
}
