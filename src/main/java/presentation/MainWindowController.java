package presentation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Account;
import model.AccountOperation;
import model.Bank;
import model.Person;

public class MainWindowController {
    @FXML
    private TableView<Person> clientsTable;

    @FXML
    private TableColumn<Person, String> clients;

    @FXML
    private ListView<Account> accounts;

    @FXML
    private RadioButton opDeposit;

    @FXML
    private RadioButton opWithdraw;

    @FXML
    private DatePicker dateField;

    @FXML
    private ToggleGroup opGroup;

    @FXML
    private TextField amountField;

    Bank bank;
    private Stage stageManageClients;
    private ManageClientController ctrlManageClients;
    private Stage stageManageAccounts;
    private ManageAccountsController ctrlManageAccounts;

    @FXML
    void open(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open File with Bank Data");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("All Files", "*.*"));
        fileChooser.setInitialDirectory(new File("."));
        File selectedFile = fileChooser.showOpenDialog(
                amountField.getScene().getWindow());
        if (selectedFile != null) {
            bank.read(selectedFile);
        }
        initListClients();
    }

    @FXML
    void save(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File with Bank Data");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("All Files", "*.*"));
        fileChooser.setInitialDirectory(new File("."));
        File selectedFile = fileChooser.showSaveDialog(
                amountField.getScene().getWindow());
        if (selectedFile != null) {
            bank.write(selectedFile);
        }
    }

    @FXML
    void closeApp(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void manageClients(ActionEvent event) {
        if (stageManageClients == null) {
            initWndManageClients();
        }
        //ctrlManageClients.initialize();
        ctrlManageClients.initializeaza();
        stageManageClients.showAndWait();
        initListClients();
    }

    @FXML
    void manageAccounts(ActionEvent event) {
        if (stageManageAccounts == null) {
            initWndManageAccounts();
        }
        ctrlManageAccounts.initializeaza();
        stageManageAccounts.showAndWait();
        Person p = clientsTable.getSelectionModel().getSelectedItem();
        if (p != null) {
            initListAccounts(p);
        }
    }

    @FXML
    void about(ActionEvent event) {
        displayInfo("Programming Techniques - Homework 4 (2017-2018)\n"
                + "Olivian Rusu (30423)",
                "Application for accounts and clients management in a bank",
                "About");

    }

    @FXML
    void operateInAccount(ActionEvent event) {
//        Person p = clients.getSelectionModel().getSelectedItem();
//        if (p == null) {
//            return;
//        }
        Account a = accounts.getSelectionModel().getSelectedItem();
        if (a == null) {
            return;
        }
        LocalDate d = dateField.getValue();
        if (d == null) {
            return;
        }
        String text = amountField.getText().trim();
        if (text.equals("")) {
            return;
        }
        float amount = -1;
        try {
            amount = Float.parseFloat(text);
        } catch (NumberFormatException e) {
        }
        if (amount <= 0) {
            MainWindowController.displayInfo("Amount should be a strictly positive float",
                    "", "Wrong value for amount");
            return;
        }
        boolean withdrawal = opWithdraw.isSelected();
        if (withdrawal) {   // cannot withdraw more than the account balance
            if (a.getBalance() - amount < 0) {
                MainWindowController.displayInfo("Amount withdrawn cannot be more"
                        + " than the balance (" + a.getBalance() + ")",
                        "", "Can't do operation");
                return;
            }
        }
        AccountOperation op = new AccountOperation(d, withdrawal, amount);
        a.addOperation(op);

        dateField.setValue(null);
        amountField.setText("");
    }

    @FXML
    void initialize() {
        bank = new Bank();
        clientsTable.getSelectionModel().selectedItemProperty().addListener(
                (obiect, valVeche, valNoua) -> {
                    Person p = (Person) valNoua;
                    initListAccounts(p);
                }
        );
        clients.setCellValueFactory(
                new PropertyValueFactory<Person, String>("name")
        );
        
        // Make column clients editable:
        clients.setCellFactory(TextFieldTableCell.forTableColumn());
        clients.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Person, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Person, String> t) {
                Person p = (Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow());
                bank.changeClientName(p, t.getNewValue());

//                if (bank.update(p)) {
//                    initTableCustomers();
//                }
            }

        });

    }

    private void initListClients() {
        Set<Person> clientsBank = bank.getClients();
        clientsTable.getItems().clear();
        clientsTable.getItems().addAll(clientsBank);
    }

    public static void displayInfo(String infoText, String header, String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, infoText);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }

    private void initListAccounts(Person p) {
        accounts.getItems().clear();
        ArrayList<Account> accnts = bank.getAccounts(p);
        if (accnts != null) {
            accounts.getItems().addAll(accnts);
        }
    }

    private void initWndManageClients() {
        try {
            // load fxml file for the window
            URL url = getClass().getResource("ManageClient.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            // load the container having the window contents
            AnchorPane container = (AnchorPane) loader.load();
            // create the object for the window:
            stageManageClients = new Stage();
            // set the title of the created window
            stageManageClients.setTitle("Clients of the bank");
            // the window will show as MODAL
            stageManageClients.initModality(Modality.APPLICATION_MODAL);
            // create the scene containing the window interface
            Scene scene = new Scene(container);
            // atach the scene (content of the window) to the stage (window)
            stageManageClients.setScene(scene);
            // get the controller object of the window
            ctrlManageClients = loader.getController();
            ctrlManageClients.setBank(bank);
        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded        
            e.printStackTrace();
        }
    }

    private void initWndManageAccounts() {
        try {
            // load fxml file for the window
            URL url = getClass().getResource("ManageAccounts.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            // load the container having the window contents
            AnchorPane container = (AnchorPane) loader.load();
            // create the object for the window:
            stageManageAccounts = new Stage();
            // set the title of the created window
            stageManageAccounts.setTitle("Manage client accounts of the bank");
            // the window will show as MODAL
            stageManageAccounts.initModality(Modality.APPLICATION_MODAL);
            // create the scene containing the window interface
            Scene scene = new Scene(container);
            // atach the scene (content of the window) to the stage (window)
            stageManageAccounts.setScene(scene);
            // get the controller object of the window
            ctrlManageAccounts = loader.getController();
            ctrlManageAccounts.setBank(bank);
        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded        
            e.printStackTrace();
        }
    }

}
